default index_date = INDEX_DATE;


"/data/mapportal.json"|
open-file|
read-json-object|
decode-json|
morph("/configs/libadminMorph.xml")|
split-entities|
change-id|
encode-esbulk(escapeChars="false", header="true", index="sb-organisations-admin-" + index_date, avoidMergers="false")|
index-esbulk(esNodes="sb-ues5.swissbib.unibas.ch:8080#sb-ues6.swissbib.unibas.ch:8080", esClustername="linked-swissbib-data-cluster", recordsPerUpload="1000");
