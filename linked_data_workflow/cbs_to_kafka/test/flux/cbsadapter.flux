// Update this url to the latest job & run.
cbs_http = "http://sb-rcbs4i.swissbib.unibas.ch:40000/JOB7/RUN5/";

cbs_http|
//if we provide a file range - always the pattern  [\d*?-\d*?] - it's not allowed to use a file name
//as part of cbs_http. Important to actually check if 167 is the file with the highest number. Will get bigger as time goes on!
cbs-export-source (fileRangePattern="job7r5A[1-167].raw")|
file-splitter(delimiter="(?<=</record>)")|
catch-object-exception|
normalize-string |
substitute-string-pattern(replace="<record>", replacement="<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\">") |
check-well-formed-xml |
encode-kafka-message(status="CREATE") |
write-kafka-configurable-writer(kafkaTopic="sb-all",bootstrapServers="sb-uka7.swissbib.unibas.ch:9092,sb-uka8.swissbib.unibas.ch:9092,sb-uka9.swissbib.unibas.ch:9092");