### Enricher Service

The enricher checks if there is a owl:sameAs field. Once the sameAs field is checked the URIs are read out.
The URIs for VIAF, GND, Wikidata and Dbpedia are then used to retrieve the associated metadata from the elasticsearch 
indices, if present.

- Link to Micro Service: [kafka-streams-enricher](https://gitlab.com/swissbib/linked/linking/kafka-streams-enricher)

If any metadata is retrieved the enrichment process begins.

The enrichment happens based on a mapping file for [organisations](prod/configs/VERZ_organisations_20191104_jow.csv) and 
a mapping file for [persons](prod/configs/VERZ_persons_20191104_jow.csv).

Each field is checked and enriched based on the specifications in this mapping. Additionally URIs in the data which fit 
one of the five data sets are matched against the indices as well. If these are present the labels are enriched in 
german, french, italian and english.

After the enrichment process they are indexed in the elasticsearch cluster.