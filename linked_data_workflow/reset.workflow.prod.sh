#!/usr/bin/env bash

echo "Are you sure that you want to reset the linked data workflow?"
echo "This will stop all services of this workflow and delete the kafka topics."

echo "Do you want to proceed? (yes/no)"
read answer

if [[ ${answer} == "yes" ]]
then
  docker stack rm swissbib-baseline swissbib-linker swissbib-enricher
  kafka-topics delete sb-all sb-categorized sb-linker-source sb-enricher-source sb-enricher-sink sb-resources-sink
fi