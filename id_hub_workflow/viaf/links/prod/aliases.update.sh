#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"rero-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"rero-viaf-links\" } },\
{ \"add\" : { \"index\" : \"wikipedia-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"wikipedia-viaf-links\" } }\
]\
}"\

