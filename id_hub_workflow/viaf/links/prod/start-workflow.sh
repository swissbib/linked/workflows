#!/usr/bin/env bash

source ../../env.sh

mkdir -p ${VIAF_IMPORT_DOWNLOAD_DIR}/tmp
mkdir -p ${VIAF_IMPORT_LINKS_DIR}

ELASTIC_HOST=http://sb-ues5.swissbib.unibas.ch:8080

curl -XPUT -H "Content-Type: application/json" --data "@./mapping/rero.mapping.json" "$ELASTIC_HOST/rero-viaf-links-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mapping/wikipedia.mapping.json" "$ELASTIC_HOST/wikipedia-viaf-links-${VIAF_DUMP_DATE}"

curl -X POST "$ELASTIC_HOST/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":[\
{ \"add\" : { \"index\" : \"rero-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"rero-viaf-links-next\" } },\
{ \"add\" : { \"index\" : \"wikipedia-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"wikipedia-viaf-links-next\" } }\
]}"


FILE="$VIAF_IMPORT_DOWNLOAD_DIR/tmp/viaf-$VIAF_DUMP_DATE-links.txt.gz"

if [[ -f "$FILE" ]]; then
    echo "latest file has already been downloaded!"
else
    echo "downloading latest links file $FILE."
    wget -q -O "$FILE" http://viaf.org/viaf/data/viaf-${VIAF_DUMP_DATE_COMPACT}-links.txt.gz
    mv "$VIAF_IMPORT_DOWNLOAD_DIR/tmp/viaf-$VIAF_DUMP_DATE-links.txt.gz" "$VIAF_IMPORT_LINKS_DIR/viaf-$VIAF_DUMP_DATE-links.txt.gz"
fi
