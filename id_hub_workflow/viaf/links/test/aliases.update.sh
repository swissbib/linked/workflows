#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"test-rero-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"test-rero-viaf-links\" } },\
{ \"add\" : { \"index\" : \"test-wikipedia-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"test-wikipedia-viaf-links\" } }\
]\
}"\

