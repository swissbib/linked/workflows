#!/usr/bin/env bash

source ../../env.sh

kafka-topics-test create viaf-all
kafka-topics-test create viaf-categorized

mkdir -p ${TEST_VIAF_IMPORT_TRIPLES_DIR}

FILE="$TEST_VIAF_IMPORT_TRIPLES_DIR/viaf-$VIAF_DUMP_DATE_COMPACT-clusters-rdf.nt.gz"

if [[ -f "$FILE" ]]; then
    echo "latest file has already been downloaded!"
else
  echo "downloading latest viaf data dump!"
  wget -q -O "$FILE" "http://viaf.org/viaf/data/viaf-$VIAF_DUMP_DATE_COMPACT-clusters-rdf.nt.gz"
fi


ELASTIC_HOST=http://sb-ues5.swissbib.unibas.ch:8080

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/organisations.json" "$ELASTIC_HOST/test-viaf-organisations-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/places.json" "$ELASTIC_HOST/test-viaf-places-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/events.json" "$ELASTIC_HOST/test-viaf-events-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/persons.json" "$ELASTIC_HOST/test-viaf-persons-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/creative-works.json" "$ELASTIC_HOST/test-viaf-creative-works-${VIAF_DUMP_DATE}"

curl -X POST "$ELASTIC_HOST/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":[\
{ \"add\" : { \"index\" : \"test-viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"test-viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"test-viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-persons-next\" } }\
]}"


sed "s/__VIAF_DUMP_DATE__/$VIAF_DUMP_DATE/g;s/__VIAF_DUMP_DATE_COMPACT__/$VIAF_DUMP_DATE_COMPACT/g" ./configs/template.properties > app.properties

mkdir -p ${TEST_VIAF_IMPORT_CONFIGS_DIR}/sort && mv app.properties "$_/"

ssh "swissbib@sb-ust1.swissbib.unibas.ch" "/swissbib_index/apps/flink/bin/flink run /swissbib_index/apps/viaf-grouped-ntriples-assembly-1.0.1.jar --config $TEST_VIAF_IMPORT_CONFIGS_DIR/sort/app.properties" &
