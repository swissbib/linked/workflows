### GND Import Workflow

The scripts and configurations for the GND-Import Workflow. This workflow can import all
the data from the lobid-gnd API as JSON-LD and indexes the data into the elastic cluster.

There are three long running services and the downloader micro service. The long running services
can be started with `deploy-services.sh`. They can run at all times and only need to be restarted on failures.

The mapping for the rdf:types to the indices can be found in [index_mappings.json](configs/index_mapping.json). 
When this file is changed the services need to be redeployed!

The downloader can be started as a container with [start-download.sh](start-download.sh). This will download the entire
gnd-data set from that day. The service creates the necessary indices on the elastic cluster.

Once all data is indexed use [alias.update.sh](alias.update.sh) to update the aliases. Other services will
now use the new data.

**IMPORTANT: Do not forget to change the dates inside the `alias.update.sh`!**