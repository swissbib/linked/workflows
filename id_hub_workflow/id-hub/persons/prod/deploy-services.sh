#!/usr/bin/env bash

kafka-topics create id-hub-persons-source
kafka-topics create id-hub-persons-sink

docker stack deploy -c docker-compose.yml id-hub-persons