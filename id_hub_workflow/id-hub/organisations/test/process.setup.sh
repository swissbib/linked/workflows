#!/usr/bin/env bash

source ../../env.sh

export ELASTIC_INDEX=test-id-hub-organisations-${ID_HUB_DATE}
export EXPORT_DATE=${ID_HUB_DATE}

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/id-hub.json" "http://sb-ues5.swissbib.unibas.ch:8080/$ELASTIC_INDEX"

mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/gnd && cp ./configs/app.gnd.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/dbpedia && cp ./configs/app.dbpedia.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/viaf && cp ./configs/app.viaf.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/wikidata && cp ./configs/app.wikidata.properties "$_/app.properties"