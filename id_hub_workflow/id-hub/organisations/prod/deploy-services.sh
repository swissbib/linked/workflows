#!/usr/bin/env bash

kafka-topics create id-hub-organisations-source
kafka-topics create id-hub-organisations-sink

docker stack deploy -c docker-compose.yml id-hub-organisations