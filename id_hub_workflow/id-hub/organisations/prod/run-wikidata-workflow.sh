#!/usr/bin/env bash

source ../../env.sh
source process.setup.sh

docker-compose -f docker-compose.wikidata.yml up -d