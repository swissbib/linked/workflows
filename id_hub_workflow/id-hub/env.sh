#!/usr/bin/env bash


export ID_HUB_CONFIGS=/swissbib_index/nas/id_hub/configs
export ID_HUB_CONFIGS_PERSONS=${ID_HUB_CONFIGS}/persons
export ID_HUB_CONFIGS_ORGANISATIONS=${ID_HUB_CONFIGS}/organisations

export ID_HUB_OLD_DATE="2020-02-25"
export ID_HUB_DATE="2020-04-28"