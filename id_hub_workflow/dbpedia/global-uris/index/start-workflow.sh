#!/usr/bin/env bash

source ../../env.sh

# TODO: check if globals ID have been updated and update env.sh as necessary!

rm /swissbib_index/nas/dbpedia/uris/global-ids-base58.tsv.bz2
wget -O /swissbib_index/nas/dbpedia/uris/global-ids-base58.tsv.bz2 "https://downloads.dbpedia.org/repo/lts/id-management/global-ids/$DBPEDIA_GLOBAL_URIS_DUMP_DATE_DOTS/global-ids-base58.tsv.bz2"

curl -XPUT -H "Content-Type: application/json" --data "@./configs/mapping.json" "http://sb-ues5.swissbib.unibas.ch:8080/dbpedia-uris-$DBPEDIA_GLOBAL_URIS_DUMP_DATE"

mkdir -p ${URIS_CONFIG_PATH}/sort && cp ./configs/app.flink.test.properties "$_/app.properties"

ssh "swissbib@sb-ust1.swissbib.unibas.ch" "/swissbib_index/apps/flink/bin/flink run /swissbib_index/apps/dbpedia-collect-id-groups-assembly-1.0.0.jar --config /swissbib_index/nas/configurations/dbpedia/uris/sort/app.properties" &
