#!/usr/bin/env bash

# TODO: Update this index to the actual current dbpedia uris index.
export DBEPDIA_URIS_INDEX_ALIAS=dbpedia-uris

kafka-topics create dbpedia-uris-wf-source
kafka-topics create dbpedia-uris-wf-sink

docker stack deploy -c docker-compose.services.yml dbpedia-global-uris
