#!/usr/bin/env bash

source ../env.sh

FLINK_JOBMANAGER_HOST=swissbib@sb-ust1.swissbib.unibas.ch
FLINK_BIN_PATH=/swissbib_index/apps/flink/bin/flink
JOB_JAR=/swissbib_index/apps/dbpedia-grouped-ntriples-assembly-1.0.3.jar

mkdir -p ${DOWNLOADER_CONFIG_PATH} && cp ./configs/download/* "$_/"
mkdir -p ${DOWNLOAD_DATA_BASE_PATH}/de
mkdir -p ${DOWNLOAD_DATA_BASE_PATH}/en
mkdir -p ${DOWNLOAD_DATA_BASE_PATH}/fr
mkdir -p ${DOWNLOAD_DATA_BASE_PATH}/it
mkdir -p ${DOWNLOAD_DATA_BASE_PATH}/commons

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/persons.json" "http://sb-ues5.swissbib.unibas.ch:8080/dbpedia-persons-$DBPEDIA_DUMP_DATE"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/organisations.json" "http://sb-ues5.swissbib.unibas.ch:8080/dbpedia-organisations-$DBPEDIA_DUMP_DATE"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/events.json" "http://sb-ues5.swissbib.unibas.ch:8080/dbpedia-events-$DBPEDIA_DUMP_DATE"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/others.json" "http://sb-ues5.swissbib.unibas.ch:8080/dbpedia-others-$DBPEDIA_DUMP_DATE"

docker-compose -f docker-compose.downloader.yml up
docker-compose -f docker-compose.downloader.yml down

mkdir -p ${SORTER_CONFIG_PATH}
sed "s/__DBPEDIA_DUMP_DATE__/$DBPEDIA_DUMP_DATE/g" ./configs/template.properties > ${SORTER_CONFIG_PATH}/app.properties

ssh ${FLINK_JOBMANAGER_HOST} "$FLINK_BIN_PATH run $JOB_JAR --config $SORTER_CONFIG_PATH/app.properties" &