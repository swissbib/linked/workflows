#!/usr/bin/env bash

kafka-topics create dbpedia-all
kafka-topics create dbpedia-categorized

docker stack deploy -c docker-compose.yml dbpedia-import
