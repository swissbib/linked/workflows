<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright (C) 2019  Project swissbib <https://swissbib.org>
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<metamorph xmlns="http://www.culturegraph.org/metamorph" version="1">

    <macros>
        <macro name="language-tag">
            <entity name="$[abbrev]" flushWith="$[predicate]" reset="true">
                <combine name="${lang}" value="${value}" flushWith="$[predicate]">
                    <data name="value" source="$[predicate]">
                        <replace pattern='##@.+' with=''/>
                    </data>
                    <data name="lang" source="$[predicate]">
                        <regexp match='##@([^ ]+)' format="${1}"/>
                    </data>
                </combine>
            </entity>
        </macro>
        <macro name="filter-language-tag">
            <combine name="$[tempName]" value="${value}" flushWith="$[predicate]" reset="true">
                <if>
                    <data source="$[predicate]">
                        <split delimiter="##"/>
                        <occurrence only="moreThan 1"/>
                        <whitelist>
                            <entry name="@en"/>
                            <entry name="@en-gb"/>
                            <entry name="@en-ca"/>
                            <entry name="@de"/>
                            <entry name="@de-ch"/>
                            <entry name="@de-at"/>
                            <entry name="@it"/>
                            <entry name="@fr"/>
                        </whitelist>
                    </data>
                </if>
                <data source="$[predicate]" name="value"/>
            </combine>
        </macro>
        <macro name="remove-datatype">
            <data source="$[predicate]" name="$[abbrev]">
                <split delimiter="##"/>
                <occurrence only="lessThan 2"/>
            </data>
        </macro>
    </macros>

    <rules>
        <data source="_id" name="\@id"/>

        <data source="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" name="\@type"/>

        <!-- only processed once per record. -->
        <data source="_id" name="\@context">
            <constant value="https://resources.swissbib.ch/wikidata/item/context.jsonld"/>
        </data>
        <!-- Wikidata -->
        <!-- Image -->
        <data source="http://www.wikidata.org/prop/direct/P18" name="wdt:P18"/>
        <!-- place of birth -->
        <data source="http://www.wikidata.org/prop/direct/P19" name="wdt:P19"/>
        <!-- place of death -->
        <data source="http://www.wikidata.org/prop/direct/P20" name="wdt:P20"/>
        <!-- gender or sex -->
        <data source="http://www.wikidata.org/prop/direct/P21" name="wdt:P21"/>
        <!-- father -->
        <data source="http://www.wikidata.org/prop/direct/P22" name="wdt:P22"/>
        <!-- mother -->
        <data source="http://www.wikidata.org/prop/direct/P25" name="wdt:P25"/>
        <!-- spouse -->
        <data source="http://www.wikidata.org/prop/direct/P26" name="wdt:P26"/>
        <!-- country of citizenship -->
        <data source="http://www.wikidata.org/prop/direct/P27" name="wdt:P27"/>
        <!-- instance of -->
        <data source="http://www.wikidata.org/prop/direct/P31" name="wdt:P31"/>
        <!-- position held -->
        <data source="http://www.wikidata.org/prop/direct/P39" name="wdt:P39"/>
        <!-- child -->
        <data source="http://www.wikidata.org/prop/direct/P40" name="wdt:P40"/>
        <!-- educated at -->
        <data source="http://www.wikidata.org/prop/direct/P69" name="wdt:P69"/>
        <!-- member of political party -->
        <data source="http://www.wikidata.org/prop/direct/P102" name="wdt:P102"/>
        <!-- native language -->
        <data source="http://www.wikidata.org/prop/direct/P103" name="wdt:P103"/>
        <!-- occupation -->
        <data source="http://www.wikidata.org/prop/direct/P106" name="wdt:P106"/>
        <!-- employer -->
        <data source="http://www.wikidata.org/prop/direct/P108" name="wdt:P108"/>
        <!-- link to picture of signature -->
        <data source="http://www.wikidata.org/prop/direct/P109" name="wdt:P109"/>
        <!-- place of burial -->
        <data source="http://www.wikidata.org/prop/direct/P119" name="wdt:P119"/>
        <!-- movement -->
        <data source="http://www.wikidata.org/prop/direct/P135" name="wdt:P135"/>
        <!-- genre -->
        <data source="http://www.wikidata.org/prop/direct/P136" name="wdt:P136"/>
        <!-- religion -->
        <data source="http://www.wikidata.org/prop/direct/P140" name="wdt:P140"/>
        <!-- award received -->
        <data source="http://www.wikidata.org/prop/direct/P166" name="wdt:P166"/>
        <!-- ethnic group -->
        <data source="http://www.wikidata.org/prop/direct/P172" name="wdt:P172"/>
        <!-- ISNI ID -->
        <data source="http://www.wikidata.org/prop/direct/P213" name="wdt:P213"/>
        <data source="http://www.wikidata.org/prop/direct/P213" name="owl:sameAs">
            <replace pattern=" " with=""/>
            <compose prefix="http://isni.org/isni/"/>
            <trim/>
        </data>
        <!-- VIAF ID -->
        <data source="http://www.wikidata.org/prop/direct/P214" name="wdt:P214"/>
        <!-- VIAF URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P214" name="wdtn:P214"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P214" name="owl:sameAs"/>
        <!-- GND ID -->
        <data source="http://www.wikidata.org/prop/direct/P227" name="wdt:P227"/>
        <!-- GND URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P227" name="wdtn:P227"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P227" name="owl:sameAs"/>
        <!-- LoC ID -->
        <data source="http://www.wikidata.org/prop/direct/P244" name="wdt:P244"/>
        <!-- LoC URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P244" name="wdtn:P244"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P244" name="owl:sameAs"/>
        <!-- ULAN ID -->
        <data source="http://www.wikidata.org/prop/direct/P245" name="wdt:P245"/>
        <!-- ULAN URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P245" name="wdtn:P245"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P245" name="owl:sameAs"/>
        <!-- BNF ID -->
        <data source="http://www.wikidata.org/prop/direct/P268" name="wdt:P268"/>
        <!-- BNF URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P268" name="wdtn:P268"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P268" name="owl:sameAs"/>
        <!-- SUDOC ID -->
        <data source="http://www.wikidata.org/prop/direct/P269" name="wdt:P269"/>
        <!-- SUDOC URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P269" name="wdtn:P269"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P269" name="owl:sameAs"/>
        <!-- IMDB ID-->
        <data source="http://www.wikidata.org/prop/direct/P345" name="wdt:P345"/>
        <!-- IMDB URI -->
        <data source="http://www.wikidata.org/prop/direct/P345" name="owl:sameAs">
            <compose prefix="https://www.imdb.com/name/"/>
            <trim/>
        </data>
        <!-- NDL ID (National Diet Library (Japan))-->
        <data source="http://www.wikidata.org/prop/direct/P349" name="wdt:P349"/>
        <!-- NDL URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P349" name="wdtn:P349"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P349" name="owl:sameAs"/>
        <!-- Commons category -->
        <data source="http://www.wikidata.org/prop/direct/P373" name="wdt:P373"/>
        <!-- partner -->
        <data source="http://www.wikidata.org/prop/direct/P451" name="wdt:P451"/>
        <!-- ORCID ID -->
        <data source="http://www.wikidata.org/prop/direct/P496" name="wdt:P496"/>
        <!-- ORCID URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P496" name="wdtn:P496"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P496" name="owl:sameAs"/>
        <!-- CBDB ID (China) -->
        <data source="http://www.wikidata.org/prop/direct/P497" name="wdt:P497"/>
        <!-- CBDB URI (China) -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P497" name="wdtn:P497"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P497" name="owl:sameAs"/>
        <!-- cause of death -->
        <data source="http://www.wikidata.org/prop/direct/P509" name="wdt:P509"/>
        <!-- date of birth -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P569" abbrev="wdt:P569"/>
        <!-- extract birth year -->
        <data source="http://www.wikidata.org/prop/direct/P569" name="dbo:deathYear">
            <regexp match="^(-?\d+)-\d{2}-\d{2}T(.*)Z$" format="${1}"/>
            <not-equals string=""/>
        </data>
        <!-- date of death -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P570" abbrev="wdt:P570"/>
        <!-- extract death year -->
        <data source="http://www.wikidata.org/prop/direct/P570" name="dbo:deathYear">
            <regexp match="^(-?\d+)-\d{2}-\d{2}T(.*)Z$" format="${1}"/>
            <not-equals string=""/>
        </data>
        <!-- Open Library ID -->
        <data source="http://www.wikidata.org/prop/direct/P648" name="wdt:P648"/>
        <!-- Open Library URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P648" name="wdtn:P648"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P648" name="owl:sameAs"/>
        <!-- NKCR ID (National Library of Czech Republic) -->
        <data source="http://www.wikidata.org/prop/direct/P691" name="wdt:P691"/>
        <!-- family name (as object) -->
        <data source="http://www.wikidata.org/prop/direct/P734" name="wdt:P734"/>
        <!-- given name (as object) -->
        <data source="http://www.wikidata.org/prop/direct/P735" name="wdt:P735"/>
        <!-- influenced by -->
        <data source="http://www.wikidata.org/prop/direct/P737" name="wdt:P737"/>
        <!-- pseudonym-->
        <data source="http://www.wikidata.org/prop/direct/P742" name="wdt:P742"/>
        <!-- notable work-->
        <data source="http://www.wikidata.org/prop/direct/P800" name="wdt:P800"/>
        <!-- KB ID (National Library Sweden)-->
        <data source="http://www.wikidata.org/prop/direct/P906" name="wdt:P906"/>
        <!-- KB URI (National Library Sweden)-->
        <data source="http://www.wikidata.org/prop/direct-normalized/P906" name="wdtn:P906"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P906" name="owl:sameAs"/>
        <!-- work location / Wirkungsort-->
        <data source="http://www.wikidata.org/prop/direct/P937" name="wdt:P937"/>
        <!-- BNE ID (Spanien)-->
        <data source="http://www.wikidata.org/prop/direct/P950" name="wdt:P950"/>
        <!-- BNE URI (Spanien)-->
        <data source="http://www.wikidata.org/prop/direct-normalized/P950" name="wdtn:P950"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P950" name="owl:sameAs"/>
        <!-- NTAN ID (Dutch Thesaurus)-->
        <data source="http://www.wikidata.org/prop/direct/P1006" name="wdt:P1006"/>
        <!-- NTAN URI (Dutch Thesaurus)-->
        <data source="http://www.wikidata.org/prop/direct-normalized/P1006" name="wdtn:P1006"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P1006" name="owl:sameAs"/>
        <!-- BIBSYS ID (Norwegen) -->
        <data source="http://www.wikidata.org/prop/direct/P1015" name="wdt:P1015"/>
        <!-- BIBSYS URI (Norwegen) -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P1015" name="wdtn:P1015"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P1015" name="owl:sameAs"/>
        <!-- relative -->
        <data source="http://www.wikidata.org/prop/direct/P1038" name="wdt:P1038"/>
        <!-- medical condition -->
        <data source="http://www.wikidata.org/prop/direct/P1050" name="wdt:P1050"/>
        <!-- manner of death -->
        <data source="http://www.wikidata.org/prop/direct/P1196" name="wdt:P1196"/>
        <!-- described by source -->
        <data source="http://www.wikidata.org/prop/direct/P1343" name="wdt:P1343"/>
        <!-- participant of -->
        <data source="http://www.wikidata.org/prop/direct/P1344" name="wdt:P1344"/>
        <!-- floruit
            date when the person was known to be active or alive, when birth or death not documented
         -->
        <call-macro name="remove-datatype" predicate="http://www.wikidata.org/prop/direct/P1317" abbrev="wdt:P1317"/>
        <!-- language spoken, written or signed -->
        <data source="http://www.wikidata.org/prop/direct/P1412" name="wdt:P1412"/>
        <!-- birth name-->
        <call-macro name="filter-language-tag" predicate="http://www.wikidata.org/prop/direct/P1477" tempName="@P1477Filtered"/>
        <call-macro name="language-tag" predicate="@P1477Filtered" abbrev="wdt:P1477"/>
        <!-- name in native language-->
        <call-macro name="filter-language-tag" predicate="http://www.wikidata.org/prop/direct/P1559" tempName="@P1559Filtered"/>
        <call-macro name="language-tag" predicate="@P1559Filtered" abbrev="wdt:P1559"/>
        <!-- CERL ID -->
        <data source="http://www.wikidata.org/prop/direct/P1871" name="wdt:P1871"/>
        <!-- Project Gutenberg ID -->
        <data source="http://www.wikidata.org/prop/direct/P1938" name="wdt:P1938"/>
        <!-- Project Gutenberg URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P1938" name="wdtn:P1938"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P1938" name="owl:sameAs"/>
        <!-- FAST ID -->
        <data source="http://www.wikidata.org/prop/direct/P2163" name="wdt:P2163"/>
        <!-- FAST URI -->
        <data source="http://www.wikidata.org/prop/direct-normalized/P2163" name="wdtn:P2163"/>
        <data source="http://www.wikidata.org/prop/direct-normalized/P2163" name="owl:sameAs"/>
        <!-- SNAC Ark ID -->
        <data source="http://www.wikidata.org/prop/direct/P3430" name="wdt:P3430"/>
        <data source="http://www.wikidata.org/prop/direct/P3430" name="owl:sameAs">
            <compose prefix="http://snaccooperative.org/ark:/99166/"/>
            <trim/>
        </data>
        <!-- sibling-->
        <data source="http://www.wikidata.org/prop/direct/P3373" name="wdt:P3373"/>
        <!-- last words before death -->
        <call-macro name="filter-language-tag" predicate="http://www.wikidata.org/prop/direct/P3909" tempName="@P3909Filtered"/>
        <call-macro name="language-tag" predicate="@P3909Filtered" abbrev="wdt:P3909"/>

        <!-- rdfs:label -->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2000/01/rdf-schema#label" tempName="@rdfsLabelFiltered"/>
        <call-macro name="language-tag" predicate="@rdfsLabelFiltered" abbrev="rdfs:label"/>

        <!-- skos:prefLabel-->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2004/02/skos/core#prefLabel" tempName="@prefLabelFiltered"/>
        <call-macro name="language-tag" predicate="@prefLabelFiltered" abbrev="skos:prefLabel"/>
        <!-- skos:altLabel -->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2004/02/skos/core#altLabel" tempName="@altLabelFiltered"/>
        <call-macro name="language-tag" predicate="@altLabelFiltered" abbrev="skos:altLabel"/>

        <!-- schema:name -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/name" tempName="@schemaNameFiltered"/>
        <call-macro name="language-tag" predicate="@schemaNameFiltered" abbrev="schema:name"/>
        <!-- schema:description -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/description" tempName="@schemaDescriptionFiltered"/>
        <call-macro name="language-tag" predicate="@schemaDescriptionFiltered" abbrev="schema:description"/>
    </rules>

</metamorph>