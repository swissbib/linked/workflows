## Wikidata Import Process

This process downloads the latest wikidata dump and imports the data into elasticsearch.

All dumps provided by wikidata can be found [here](https://dumps.wikimedia.org/wikidatawiki/entities/). We 
only use the [latest-truthy-dump](https://dumps.wikimedia.org/wikidatawiki/entities/latest-truthy.nt.bz2).

This dump contains only a subset of triples for each wikidata item as they do 
not contain references and qualifiers. The advantage is that it contains considerably less triples, making
the dump smaller. The disadvantage is that some data loses its meaning (such as dates with only the year specified 
become 01-01-YYYY).

Importing other dumps might be a possibility in the future.

The csv files are included with an volume, because they are too big for docker confg (max. 512 Kb).