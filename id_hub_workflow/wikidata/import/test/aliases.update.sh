#!/usr/bin/env bash

NEW_DUMP_DATE=2019-12-14

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"test-wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"test-wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-all\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"test-wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-all\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-persons\" } },\
{ \"remove\" : { \"index\" : \"test-wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-persons-next\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-all\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-items-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-items\" } },\
{ \"add\" : { \"index\" : \"test-wikidata-items-${NEW_DUMP_DATE}\", \"alias\" : \"test-wikidata-all\" } }\
]\
}"