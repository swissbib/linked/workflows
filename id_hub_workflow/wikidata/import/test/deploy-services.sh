#!/usr/bin/env bash

TEST_WIKIDATA_PATH=/swissbib_index/nas/test/wikidata
export TEST_WIKIDATA_CONFIGS="$TEST_WIKIDATA_PATH/configs"

mkdir -p ${TEST_WIKIDATA_PATH}/dump
mkdir -p ${TEST_WIKIDATA_PATH}/meta && touch "$_/read_files.txt"

mkdir -p ${TEST_WIKIDATA_CONFIGS} && cp ./configs/events.csv ./configs/organisations.csv "$_"

kafka-topics-test create wikidata-all
kafka-topics-test create wikidata-filtered
kafka-topics-test create wikidata-categorized

docker stack deploy -c docker-compose.yml test-wikidata-import