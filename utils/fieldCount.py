#!/usr/bin/python3
# -*- coding: utf-8 -*-

# checks the number of documents which have a value in each field in elasticsearch
# usage: python fieldCount.py NAME_OF_INDEX
from elasticsearch import Elasticsearch
import sys

if __name__ == '__main__':
    es = Elasticsearch(
        ['localhost'],
        port=8080,
    )
    index = sys.argv[1]
    results = dict()
    mapping = es.indices.get_mapping(index)

    total_count = es.count(index=index)['count']
    print(total_count)
    for field in mapping[index]['mappings']['properties']:
        query = {
            "query": {
                "exists": {"field": field}
            }
        }
        result = es.count(index=index, body=query)
        results[field] = result['count']

    with open(f'{index}.csv', 'w') as fp:
        fp.write("field,field_count\n")

        for r in sorted(results.items(), key=lambda item: item[1], reverse=True):
            fp.write(f"{r[0]},{r[1]}\n")









