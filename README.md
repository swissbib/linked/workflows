# Id Hub Processes

The id hub is generated from four external datasets:

- [VIAF](http://viaf.org/viaf/data/)
- [lobid GND](http://lobid.org/gnd)
- [Wikidata](https://dumps.wikimedia.org/wikidatawiki/entities/)
- [Dbpedia](https://databus.dbpedia.org/)

Before the id hub can be generated each data source must be updated. Each dataset has a different update 
schedule. 

- VIAF is updated once a month. A new link becomes available sometime in the first week of each month.
- GND is updated daily and can be queried anytime.
- Wikidata dump is updated every few days. The file has the http header `Last-Modified` to check.
- Dbpedia is currently not updated, but will be monthly once ready... (check [databus.dbpedia.org](https://databus.dbpedia.org/) for updates.)

## Overview

![workflow overview image](workflow_diagramm.png "Overview Image")

All images can be updated on [draw.io](https://draw.io) by opening the *.drawio files with the same names as the 
PNG files.

## Wikidata Ingest Process

![wikidata ingest process overview](id_hub_workflow/wikidata/import/wikidata_ingest_process.png "Wikidata Ingest Process")

The Wikidata ingest process has five steps: downloading, producing, filtering, normalizing and ingest. There is a
micro service for each step. 

The wikidata dataset can be downloaded from 
[Wikidata Wiki Entities](https://dumps.wikimedia.org/wikidatawiki/entities/). The file relevant for us 
is the [latest-truthy.nt.bz2](https://dumps.wikimedia.org/wikidatawiki/entities/latest-truthy.nt.bz2). 
This file will be updated every time a new dump is available.

The workflow is started with the [start.workflow.sh](./id_hub_workflow/wikidata/import/prod/start.workflow.sh) script.

Currently the entire process is too slow to import every release published by wikimedia! As a result the downloader
does no longer automatically get new dumps.

### Step 1: Wikidata Downloader

- Link to Micro Service: [Wikidata Downloader](https://gitlab.com/swissbib/linked/linking/download-wikidata)
- Link to Deployment: [Start Workflow](./id_hub_workflow/wikidata/import/prod/start.workflow.sh)
- Link to Configurations: [Wikidata Downloader Configurations](./id_hub_workflow/wikidata/import/prod/configs/download)

Wikimedia publishes a new dump once every few days. The releases are not on fixed dates. 
We use the [latest-truthy.nt.bz2](https://dumps.wikimedia.org/wikidatawiki/entities/latest-truthy.nt.bz2) 
file as basis for the import. Each time an update is posted the http-header `Last-Modified` changes. 
Once the Last-Modified header is higher than the timestamp stored in zookeeper a new dump is downloaded.

### Step 2: Wikidata Producer

- Link to Micro Service: [Kafka Files Producer](https://gitlab.com/swissbib/linked/kafka-producer-files)
- Link to Deployment: [Deploy Services](./id_hub_workflow/wikidata/import/prod/deploy-services.sh)
- Link to Configuration: [Configuration](./id_hub_workflow/wikidata/import/prod/configs/app.producer.properties)

Watches the download folder for new completed dumps. Each time a new file is put into this location 
the file is read and ingested into kafka.

### Step 3: Wikidata Filter

- Link to Micro Service: [Wikidata Import Filter](https://gitlab.com/swissbib/linked/linking/wikidata-import-filter)
- Link to Deployment: [Deploy Services](./id_hub_workflow/wikidata/import/prod/deploy-services.sh)
- Link to Configuration: [Configuration](./id_hub_workflow/wikidata/import/prod/configs/app.filter.properties)

A micro service which first filters out all dataset messages and then filters out alls skos:prefLabel and rdfs:label 
triples. The names are equally defined in schema:name which is kept! This micros service has the purpose 
of making the importer faster.

### Step 4: Wikidata Import

- Link to Micro Service: [Wikidata Import](https://gitlab.com/swissbib/linked/linking/kafka-streams-wikidata-entities)
- Link to Deployment: [Deploy Services](./id_hub_workflow/wikidata/import/prod/deploy-services.sh)
- Link to Configuration: [Configuration](./id_hub_workflow/wikidata/import/prod/configs/app.stream.properties)

In a first step the process determines the type of the document. Based on this it is determined which metafacture
pipeline is used:

- Persons are determined by the fact that they are all humans.
- Organisations are determined based on the list found in [organisations.csv](./id_hub_workflow/wikidata/import/prod/configs/organisations.csv)
- Events are determined based on the list found in [events.csv](./id_hub_workflow/wikidata/import/prod/configs/events.csv)
- Items are all documents with `rdf:type <http://wikiba\.se/ontology#Item>` and which were not matched before.
- All other messages are discarded.

The lists for events and organisation must be updated from time to time!

This can be done with the [Wikipedia Query Service](https://query.wikidata.org/)

Organisations
```sparql
SELECT ?organisationSubClasses
WHERE
{
	?organisationSubClasses wdt:P279* wd:Q43229.
}
```
Events
```sparql
SELECT ?eventsSubClasses
WHERE
{
	?eventsSubClasses wdt:P279* wd:Q1656682.
}
```
Download the resulting table as CSV and overwrite the existing file. **Do not forget to delete the label from the first 
row!**

Transforms each wikidata resource from a collection of n-triples to a JSON-LD document. In this transformation process 
many properties are filtered, all properties are shortened and some data normalization takes place!

The transformation is done inside of a metafacture pipe. At the core of each pipe is a morph file. Each morph file is 
applied to a different type of wikidata resource:

- [Humans](./id_hub_workflow/wikidata/import/prod/configs/humanMorph.xml)
- [Organisations](./id_hub_workflow/wikidata/import/prod/configs/organisationMorph.xml)
- [Events](./id_hub_workflow/wikidata/import/prod/configs/eventsMorph.xml)
- [Items](./id_hub_workflow/wikidata/import/prod/configs/itemsMorph.xml)

### Step 5: Wikidata Consumer

- Link to Micro Service: [ElasticSearch Connector](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment: [Deploy Services](./id_hub_workflow/wikidata/import/prod/deploy-services.sh)
- Link to Configuration: [Configuration](./id_hub_workflow/wikidata/import/prod/configs/app.consumer.properties)

Reads the created JSON-LD Records from the Wikidata Importer and indexes them in the elasticsearch cluster.

## Virtual International Authority File (VIAF)

The workflow to download, sort, transform and then index the VIAF Dump. 
VIAF is released once a month at [Viaf Datasets](https://viaf.org/viaf/data). 
The processes here use two of the files presented:

![viaf ingest process overview](./id_hub_workflow/viaf/import/viaf_ingest_process.png)

## VIAF Ingest Process

This workflow is started with the 
[start-workflow.sh](./id_hub_workflow/viaf/import/prod/start-workflow.sh) 
Before the script is started the date inside of the 
[env.sh](./id_hub_workflow/viaf/env.sh) file has to be updated.

### Step 1.1: Download Dump

To download the VIAF dataset dump a simple wget command is used. The command is defined in the 
[start-workflow.sh](./id_hub_workflow/viaf/import/prod/start-workflow.sh) script.

Start this script in detached mode (with &) as the download takes several hours to complete.

### Step 1.2: VIAF N-Triples Sorter (Flink Job)

- Link to Micro Service: [VIAF N-Triples Sorter](https://gitlab.com/swissbib/linked/viaf-grouped-ntriples)
- Link to Configurations: [Configuration Template](./id_hub_workflow/viaf/import/prod/configs/template.properties)

This step is automatically started once the download finishes in the 
[start-workflow.sh](./id_hub_workflow/viaf/import/prod/start-workflow.sh) script.

The sorter takes the n-triple dump and sorts the entire file by subject. 
All triples of a single subject are then grouped together inside of a single message in kafka. The key
of the message is the subject URI.

For the properties `schema:name`, `schema:description` and `skos:prefLabel` can appear without language tags. These
are removed as they are assumed to be present in the metafacture pipe of the next step. Without the language tags 
elasticsearch indexing becomes impossible.

Additionally the properties `rdfs:comment` and `rdf:value` are removed.

### Step 2: VIAF Normalizer

- Link to Micro Service: [Viaf Import](https://gitlab.com/swissbib/linked/linking/kafka-streams-viaf-clusters)
- Link to Deployment: [Deploy Script](./id_hub_workflow/viaf/import/prod/deploy-services.sh)
- Link to Configurations: [Configuration](./id_hub_workflow/viaf/import/prod/configs/app.stream.properties)

A metafacture pipe and some simple transformations to create JSON-LD Records from the n-Triples messages. 
There are four different morphs and five filters as two filters share the same Morph.

- Persons: [Filter](./id_hub_workflow/viaf/import/prod/configs/filters/personFilter.xml), [Morph](./id_hub_workflow/viaf/import/prod/configs/morphs/morphPerson.xml)
- Organisations: [Filter](./id_hub_workflow/viaf/import/prod/configs/filters/organisationFilter.xml), [Morph](./id_hub_workflow/viaf/import/prod/configs/morphs/morphOrganisation.xml)
- Events: [Filter](./id_hub_workflow/viaf/import/prod/configs/filters/eventFilter.xml), [Morph](./id_hub_workflow/viaf/import/prod/configs/morphs/morphEvents.xml)
- Places: [Filter](./id_hub_workflow/viaf/import/prod/configs/filters/placeFilter.xml), [Morph](./id_hub_workflow/viaf/import/prod/configs/morphs/morph.xml)
- Creative Works: [Filter](./id_hub_workflow/viaf/import/prod/configs/filters/creativeWorkFilter.xml), [Morph](./id_hub_workflow/viaf/import/prod/configs/morphs/morph.xml)

### Step 3: VIAF Consumer

- Link to Micro Service: [ElasticSearch Connector](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment: [Deploy Script](./id_hub_workflow/viaf/import/prod/deploy-services.sh)
- Link to Configurations: [Configuration](./id_hub_workflow/viaf/import/prod/configs/app.consumer.properties)

Reads the created JSON-LD Records from the VIAF Importer and indexes them in the elasticsearch cluster.

## VIAF Links Ingest Process
This process downloads, processes and indexes viaf links in elasticsearch. These links are then used to 
connect RERO URIs with their VIAF pendants. As basis for this workflow is the 
viaf-links file found on the [VIAF Dataset](https://viaf.org/viaf/data) page. In this file
each line contains a viaf URI and a source dataset identifier.

At the moment Wikipedia links are indexed as well, but not used for anything at the moment.

![viaf links ingest process overvew](./id_hub_workflow/viaf/links/viaf_links_ingest_process.png "Viaf Links Ingest Process")

### Step 1: VIAF Links Download

The download is handled with wget inside of the 
[start-workflow.sh](./id_hub_workflow/viaf/links/prod/start-workflow.sh) script.

Once the file is downloaded it is moved into the directory watched by the producer. 

The script creates the necessary ElasticSearch indices with mappings and aliases.

### Step 2: VIAF Links Producer

- Link to Micro Service: [Kafka Files Producer](https://gitlab.com/swissbib/linked/kafka-producer-files)
- Link to Deployment: [Deploy Services](./id_hub_workflow/viaf/links/prod/deploy-services.sh)
- Link to Configuration: [Configs](./id_hub_workflow/viaf/links/prod/config/app.producer.properties)

Watches the download folder for new completed dumps. Each time a new file is put into this location the 
file is read and ingested into kafka. Each line of the files produces a new message.

### Step 3: VIAF Links Import

- Link to Micro Service: [VIAF Import](https://gitlab.com/swissbib/linked/linking/kafka-streams-viaf-links)
- Link to Deployment: [Deploy Services](./id_hub_workflow/viaf/links/prod/deploy-services.sh)
- Link to Configuration: [Configs](./id_hub_workflow/viaf/links/prod/config/app.stream.properties)

Takes each link and transforms them into a simple JSON-LD structure if they are a link to RERO or Wikipedia. 
All other links are currently ignored.

### Step 4: VIAF Links Consumer

- Link to Micro Service: [ElasticSearch Connector](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment: [Deploy Services](./id_hub_workflow/viaf/links/prod/deploy-services.sh)
- Link to Configuration: [Configs](./id_hub_workflow/viaf/links/prod/config/app.consumer.properties)

Reads the created JSON-LD Records from the VIAF Links Importer and indexes them in the elasticsearch cluster.

## Gemeinsame Normdatei (GND) Ingest Process

The GND Workflow is started with [start-download.sh](./id_hub_workflow/gnd/prod/start-download.sh) script. 
The date of the new indices is set to the day the import is started.

### Step 1: Download

- Link to Micro Service: [Downloader](https://gitlab.com/swissbib/linked/linking/download-gnd)
- Link to Deployment: [Start Download](./id_hub_workflow/gnd/prod/start-download.sh)
- Link to Configurations: [Configs](./id_hub_workflow/gnd/prod/configs/download)

The downloader can be started to load all the files to create the indices.

### Step 2: Kafka Ingest

- Link to Micro Service: [Files Producer](https://gitlab.com/swissbib/linked/kafka-producer-files)
- Link to Deployment: [Deploy Services](./id_hub_workflow/gnd/prod/deploy-services.sh)
- Link to Configurations: [Config](./id_hub_workflow/gnd/prod/configs/app.producer.properties)

The file reader is a long running micro service that watches the directory where the downloader stores the downloaded files.

### Step 3: GND Import

- Link to Micro Service: [GND Import](https://gitlab.com/swissbib/linked/linking/kafka-streams-gnd-import)
- Link to Deployment: [Deploy Services](./id_hub_workflow/gnd/prod/deploy-services.sh)
- Link to Configurations: [Config](./id_hub_workflow/gnd/prod/configs/app.stream.properties)

Applies a few transformations to the imported JSON-LD records:

1. Extract key from from id field (will be used as _id in the index).
2. The index-date is inherited from the producer.
3. The rdf:types are expanded to their full namespaces.
4. The field gender is replaced with the wikidata gender values and field (wdt:P21).
5. Extracts dbo:birthYear and dbo:deathYear from the date fields where possible.
6. URL Decodes the Dbpedia URIs in the sameAs field. Otherwise they cannot be compared to the id-hub.
7. Adds indexing metadata for the consumer (index name is based on the [index_mapping.json](https://gitlab.com/swissbib/linked/workflows/blob/master/gnd/transformer/index_mapping.json).

### Step 4: GND Consumer

- Link to Micro Service: [Elasticsearch Connector](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment: [Deploy Services](./id_hub_workflow/gnd/prod/deploy-services.sh)
- Link to Configurations: [Config](./id_hub_workflow/gnd/prod/configs/app.consumer.properties)

Consumes the transformed JSON-LD records and indexes them in the elasticsearch cluster.

## Dbpedia Ingest Prozess [WIP]

Needs updates once the dbpedia releases are stabilized.

The [start-workflow](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/start-workflow.sh) script 
currently runs the downloader and the sorter after each other. However at the moment the script loads old data 
and must be run in the foreground.

Currently only dbo:abstract and dbo:isPrimaryTopicOf are actually enriched!

The date of the release must be set in the [env.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/env.sh) 
file.

The indices are created in the start-workflow script  using the mappings defined [here](https://gitlab.com/swissbib/linked/workflows/tree/master/dbpedia/import/mappings).

### DBpedia Downloader

- Link to Micro Service: [https://gitlab.com/swissbib/linked/linking/download-dbpedia](https://gitlab.com/swissbib/linked/linking/download-dbpedia)
- Link to Deployment: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/start-workflow.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/start-workflow.sh)
- Link to Configurations: [https://gitlab.com/swissbib/linked/workflows/tree/master/dbpedia/import/configs/download](https://gitlab.com/swissbib/linked/workflows/tree/master/dbpedia/import/configs/download)

Once started the downloader uses SPARQL requests on the [Databus SPARQL Endpoint](https://databus.dbpedia.org/repo/sparql) to get the download links for all the files.

Currently loads all the generic releases and not a improved dataset. These will be available at a later date. Downloads all the files for the languages de, en, fr, it & commons.

The data loaded is not very current:

- Abstracts are from 2016
- The rest is from august / september 2019

Additionally the abstracts inside of the dataset have a encoding bug. Currently we use the abstracts found in the felix 
workflow folders. They are currently in the download folders on NAS.

### DBpedia Sorter (Flink Job)

- Link to Micro Service: [https://gitlab.com/swissbib/linked/dbpedia-grouped-ntriples](https://gitlab.com/swissbib/linked/dbpedia-grouped-ntriples)
- Link to Deployment: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/start-workflow.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/start-workflow.sh)
- Link to Configurations: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/template.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/template.properties)

The template config is edited with the date of the release. Sorts the triples of all language releases and puts them into kafka.

### Dbpedia Import

- Link to Micro Service: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources)
- Link to Deployment: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/deploy-services.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/deploy-services.sh)
- Link to Configurations: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/app.transformer.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/app.transformer.properties)

Uses metafacture commands to transform the ntriples resources into JSON-LD records. One morph per type:

- Events: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/eventsMorph.xml](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/eventsMorph.xml)
- Organisations: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/organisationsMorph.xml](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/organisationsMorph.xml)
- Persons: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/personsMorph.xml](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/personsMorph.xml)
- Others: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/othersMorph.xml](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-resources/blob/master/src/main/resources/othersMorph.xml)

Organisations are identified by rdf:type dbo:Organisation or schema:Organization.
Events are identified by rdf:type dbo:Event or schema:Event.
Persons are identified by rdf:type dbo:Person or wdt:Q5 (Human).
Others includes any resources which are not events, organisations or persons.

### Dbpedia Consumer

- Link to Micro Service: [https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/deploy-services.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/deploy-services.sh)
- Link to Configurations:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/app.consumer.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/import/configs/app.consumer.properties)

Consumes the transformed JSON-LD records and indexes them in the elasticsearch cluster.

## Dbpedia sameAs URI Import

This workflow imports the sameAs links created by the Id Managment of the Dbpedia release. These sameAs links are no 
longer present in the base data set and need to be imported separately. The process has two steps: Indexing and Merging.
For the indexing step the dataset is downloaded and then indexed in elasticsearch. In the second step this index is used 
to add the sameAs links to the dbpedia dataset.

This workflow can only be run once the dbpedia import is finished!

### Dbpedia Global URI Indexer

The global URI indexer can be run with the [start-workflow](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/start-workflow.sh)
script. It first downloads the specified id-management tsv dataset. The download location for this file will probably 
require an update. The current release is from August 2019.

The same script then starts the Flink Job to sort and transform the links into JSON Records.

- Link to Micro Service: [https://gitlab.com/swissbib/linked/dbpedia-collect-id-groups](https://gitlab.com/swissbib/linked/dbpedia-collect-id-groups)
- Link to Deployment:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/start-workflow.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/start-workflow.sh)
- Link to Configurations:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/configs/app.flink.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/configs/app.flink.properties)

The index for this data is created with this [mapping](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/configs/mapping.json).

Then the consumer takes these JSON records from kafka and indexes them:

- Link to Micro Service: [https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/deploy-services.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/deploy-services.sh)
- Link to Configurations:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/configs/app.consumer.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/index/configs/app.consumer.properties)

### DBpedia Global URI Enricher / Merger

In this second step the generated index from the first step is used to enrich the dbpedia resources with their sameAs links. This step can only be started

when the first step is finished and the dbpedia indices are ready.

These must be merged, because some sameAs links (VIAF, GND, commons) actually already exist in the dataset.

The workflow is started with the [start-workflow](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/start-workflow.sh) script. Here three producers are started which take the resources from the dbpedia-persons, -organisations and -events indices

and puts them into Kafka for processing.

- Events Producer Properties: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.events.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.events.properties)
- Organisations Producer Properties: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.organisations.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.organisations.properties)
- Persons Producer Properties: [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.persons.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.persons.properties)

There the micro service to enrich the data is used to update the sameAs relations with the values found in the index.

- Link to Micro Service: [https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-global-uris-ingest](https://gitlab.com/swissbib/linked/linking/kafka-streams-dbpedia-global-uris-ingest)
- Link to Deployment:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/deploy-services.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/deploy-services.sh)
- Link to Configurations:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.merger.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.merger.properties)

In the end the consumer updates the merged sameAs properties back into the indices with partial docs udpdate.

- Link to Micro Service: [https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)
- Link to Deployment:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/deploy-services.sh](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/deploy-services.sh)
- Link to Configurations:  [https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.consumer.properties](https://gitlab.com/swissbib/linked/workflows/blob/master/dbpedia/global-uris/merge/configs/app.consumer.properties)

# Id Hub Generation

The id hub generation is separated into a persons and organisations workflows. But these work identical and 
differ only in configurations. There is a run script for each data source (VIAF, WIKIDATA, GND, DBPEDIA). 
Before these can be started the indices of the source datasets must be ready.

### Id Hub Generation Services

Use the deploy-services script to before starting the workflow. The micro services started can be found here:

- Merger: [Id Hub Merger Service](https://gitlab.com/swissbib/linked/linking/kafka-streams-id-cluster-update-transformation)
- Consumer: [Elasticsearch Consumer](https://gitlab.com/swissbib/linked/kafka-consumer-elasticsearch)

### Id Hub Generation Producers

- Link to Micro Service: [https://gitlab.com/swissbib/linked/kafka-producer-elasticsearch](https://gitlab.com/swissbib/linked/kafka-producer-elasticsearch)

Each data source set has their own configuration and script to start. The scripts to start each workflow, 
the configuration files and the index mappings can be found here:

- [Persons](./id_hub_workflow/id-hub/persons/prod)
- [Organisations](./id_hub_workflow/id-hub/organisations/prod)

For each data source there is a separate docker-compose file and a bash script to start the ingest. Keep in mind
that the process for each data source should be started in order gnd -> viaf -> wikidata -> dbpedia. And 
can only be started once the indices with the -next sub alias are ready.

# Linked Data Workflow

The linked data workflow begins with MARCXML Records exported by CBS. The data comes from CBS in one of two ways:

- A full export
- Updates & deletes message queue

The full export can be loaded into Kafka with a flux script found here: [full export](./linked_data_workflow/cbs_to_kafka/prod)
Whenever a new export is ingested into Kafka the entire linked data workflow should be reset!

- Turn off the services run on docker swarm: swissbib-baseline, swissbib-linker, swissbib-enricher
- Delete topics: sb-all, sb-categorized, sb-linker-source, sb-enricher-source, sb-enricher-sink, sb-resources-sink

The workflow can be reset with the [reset.workflow.prod.sh](https://gitlab.com/swissbib/linked/workflows/-/blob/master/swissbib/reset.workflow.prod.sh) script.

The message queue is updated by the message catcher and can be started [here](./linked_data_workflow/message_catcher/prod/deploy.sh).

## Baseline Preprocessor

A micro service which transforms MARCXML Records.

- Link to Micro Service: [Baseline](https://gitlab.com/swissbib/linked/lsb-baseline)
- Link to Deployment: [Deploy Services](./linked_data_workflow/baseline/prod/deploy-services.sh)
- Link to Configurations: [Configs](./linked_data_workflow/baseline/prod/configs)

This is currently used to copy identifiers from 950 persons and organisations fields into their 
1XX / 7XX counterpart. 

## Baseline

The creation of linked data is a three step process. First the baseline converts CBS MARCXML records into
JSON-LD records for all resource types.

- Link to Micro Service: [Baseline](https://gitlab.com/swissbib/linked/lsb-baseline)
- Link to Deployment: [Deploy Services](./linked_data_workflow/baseline/prod/deploy-services.sh)
- Link to Configurations: [Configs](./linked_data_workflow/baseline/prod/configs)

Each resource type is generated by a morph file. The morph files can be updated in 
[here](./linked_data_workflow/baseline/prod/configs/morphs).

## Consumer

The consumer now reads and indexes all the resources with types BibliographicResource, Item and Document. 

## Linker

The persons and organisations are now read by the linker micro service. 

- Link to Micro Service: [Linker](https://gitlab.com/swissbib/linked/linking/kafka-streams-linker)
- Link to Deployment: [Deploy Services](./linked_data_workflow/linker_service/prod/deploy-services.sh)
- Link to Configurations: [Configs](./linked_data_workflow/linker_service/prod/configs/app.stream.properties)

This micro service checks if the person or organisation has an owl:sameAs relationship. In case that there is no field
owl:sameAs the record is forwarded to the enriched output topic for indexing. In the case that there is a field
owl:sameAs then each link is checked in the id hub. Both GND and RERO IDs can be present. GND IDs are normalized with
the providers namespace and compared to the identifiers.GND field in the Id Hub. THe RERO Ids are first matched in the
rero-viaf-links index to convert them into VIAF URIs. These VIAF URIs are then used to find matches in the Id Hub.

In the case that a match is found, all of the URIs present in the Id Hub cluster are added to the owl:sameAs field.
Once this has happened they are forwarded to the enricher.

## Enricher

The enricher checks if there is a owl:sameAs field. Once the sameAs field is checked the URIs are used to enrich the document.
Each data source (VIAF, GND, WIKIDATA, DBPEDIA) is checked if an URI is present.

- Link to Micro Service: [Enricher](https://gitlab.com/swissbib/linked/linking/kafka-streams-enricher)
- Link to Deployment: [Deploy Services](./linked_data_workflow/enricher_service/prod/deploy-services.sh)
- Link to Configurations: [Configs](./linked_data_workflow/enricher_service/prod/configs)

If any metadata is retrieved the enrichment process begins.

The enrichment happens based on a mapping file for [organisations](./linked_data_workflow/enricher_service/prod/configs/VERZ_organisations_20191104_jow.csv) and 
a mapping file for [persons](./linked_data_workflow/enricher_service/prod/configs/VERZ_persons_20191104_jow.csv)

Each field is checked and enriched based on the specifications in this mapping. Additionally URIs in the data which fit 
one of the five data sets are matched against the indices as well. If these are present the labels are enriched in 
german, french, italian and english.

After the enrichment process they are indexed in the elasticsearch cluster.

## LibAdmin Organisations

The libraries present in the libadmin tool are treated in a special way. They are directly imported with
the workflow [here](./linked_data_workflow/libadmin/start-workflow.sh).

This process can be started independent of any other processes.